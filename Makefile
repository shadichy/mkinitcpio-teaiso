DESTDIR=/
all: install

prepare:
	mkdir -p $(DESTDIR)/lib/initcpio/hooks
	mkdir -p $(DESTDIR)/lib/initcpio/install
	
install: prepare
	install hook_live_hook $(DESTDIR)/lib/initcpio/hooks/live_hook
	install install_live_hook $(DESTDIR)/lib/initcpio/install/live_hook
	
remove:
	rm -f $(DESTDIR)/lib/initcpio/hooks/live_hook
	rm -f $(DESTDIR)/lib/initcpio/install/live_hook
